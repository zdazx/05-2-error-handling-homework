package com.twuc.webApp.domain.mazeRender.mazeWriters;

import com.twuc.webApp.domain.mazeRender.AreaRender;
import com.twuc.webApp.domain.mazeRender.CellRender;
import com.twuc.webApp.domain.mazeRender.MazeComponentFactory;
import com.twuc.webApp.domain.mazeRender.MazeRenderSettings;
import com.twuc.webApp.domain.mazeRender.areaRenders.AreaColorRender;
import com.twuc.webApp.domain.mazeRender.cellRenders.CellColorRender;
import com.twuc.webApp.domain.mazeRender.cellRenders.TagValueGradientRender;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.util.Collections;
import java.util.List;

@Component
public class ColorSolvingMazeComponentFactory implements MazeComponentFactory {
    @Override
    public List<AreaRender> createBackgroundRenders() {
        return Collections.singletonList(new AreaColorRender(Color.WHITE));
    }

    @Override
    public List<CellRender> createWallRenders() {
        return Collections.singletonList(new CellColorRender(Color.BLACK));
    }

    @Override
    public List<CellRender> createGroundRenders() {
        return Collections.singletonList(new TagValueGradientRender(
            "resolve_distance",
            0,
            500,
            Color.WHITE,
            new Color(0, 0, 100)
        ));
    }

    @Override
    public MazeRenderSettings createSettings() {
        return new MazeRenderSettings(8, 10);
    }
}
