package com.twuc.webApp.web;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class MazeControllerExceptionHandler {
    @ExceptionHandler
    public ResponseEntity<String> handle(IllegalArgumentException exception){
        return ResponseEntity.status(500).body("should_input_correct_type");
    }
}
